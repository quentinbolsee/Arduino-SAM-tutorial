# Arduino SAM Tutorial

## 0. Installing

To install the Arduino Core, add the following URL to `Additional Boards Manager URLs`:

```
https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json
```

For Arduino IDE (Arduino v2), this is in the settings page.

<img src=img/arduino-ide.jpg width=50%>


You can then install the package by opening up the Boards Manager:

<img src=img/board_manager.png width=50%>

And looking for a package called `Fab SAM core for Arduino`. Simply install the latest version:

<img src=img/install_board.png width=50%>

A new menu appears under `board`, letting you select a variety of chips. Some generic names contain an `x`, for instance for a SAMD21E17, you would select `Generic x21E`:

<img src=img/board_list.png width=50%>

Then select the right microcontroller in the next menu:

<img src=img/microcontroller.png width=50%>

## 1. With bootloader

Because the SAMD family has native USB support, they can easily talk to the host computer using a CDC serial class. A neat trick is to use this serial port not only to communicate, but also upload your binary when needed.

This can be achieved with a bootloader, that first needs to be installed on the microcontroller using an external programmer. After this, your board will behave like an official Arduino board and can be flashed through the serial port without any programmer plugged in.

<img src=img/with_bootloader.png width=50%>

### 1.1 Burn bootloader

Connect a CMSIS-DAP adapter (Atmel ICE is also fine) to your board and make sure both are powered. Here are examples with a D11 and a D21, with various programmer/connector styles:

<img src=img/programming_d11.jpg width=50%><img src=img/programming_d21.jpg width=50%><br>

Select the right board and microcontroller, then `CMSIS-DAP` as the programmer, then click on `Burn Bootloader`.

<img src=img/burn_bootloader.png width=50%>

If all goes well, your board will now be detected as a serial port and you can start uploading to it through the bootloader.

### 1.2 Upload sketch

To upload a sketch, simply use the `upload` command or the arrow icon. Make sure the bootloader size is not set to `NO_BOOTLOADER`, otherwise the bootloader won't be used.

<img src=img/upload.png width=50%>

### 1.3 Upload with programmer (optional)

This is a special scenario where you have a bootloader but still want to upload using a programmer. Reasons might be:

- Upload is slow through the bootloader
- You disabled USB support and the board cannot be found

In those cases, connect a programmer and explicitly pick `Upload Using Programmer`:

<img src=img/upload_programmer.png width=50%>

## 2. Without bootloader

As expained previously, bootloaders use some of the flash. With the SAMD11, we only have 16kb of total flash, and the bootloader would use 4kb of that. If you want to recover this extra space, you can use your chip without a bootloader altogether. You'll need a programmer connected every time you upload a sketch.

To enable this behavior, simply select `NO_BOOTLOADER` in bootloader size:

<img src=img/without_bootloader.png width=50%>

### 2.1 Upload

To upload without a bootloader, connect a programmer and either pick `Upload` or the arrow button. `Upload Using Programmer` has the same effect.

<img src=img/upload.png width=50%>
